# Patients’ Monitoring System Using IOT

This Application was my Graduation Project 
from Faculty of Informatiom Technology - Software Engineering and Information Systems Department. 2015/2020

## Project Brief Description
An App with two types of users Doctors and Hospital Staff.
Doctors have an access to monitor the patients they are in charge of, view their information and their vital data (Heart Rate and Temperature)
Hospital staff are allowed to view the doctor’s information, patient’s information, vital data, and old records, they can also view the rooms and sections available in the hospital.
The APIs are hosted on Heroku.

![Video](Video.mp4)  