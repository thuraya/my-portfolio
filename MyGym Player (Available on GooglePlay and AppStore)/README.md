# Gymesis: Player Application V2

An application that provides services for gym members.

## Application Features
- Choose your gym
- Login
- Scan your QR Code to check in to your gym
- Workouts #(V2 Feature)
- View your memberships
- View gym classes
- View your check ins
- View your profile
- Edit profile
- Change Password
- Logout
- Push Notifications
### Get the application
- [Play Store](https://play.google.com/store/apps/details?id=com.gymesis.player_app)
- [App Store](https://apps.apple.com/app/id1558730679)


